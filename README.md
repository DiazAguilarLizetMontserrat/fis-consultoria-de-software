```plantuml
@startmindmap
<style>
mindmapDiagram {
    node {
     BackgroundColor gray
     HorizontalAlignment center
    }
    .sep {
      BackGroundColor yellow
    }
  }
</style>
*[#skyblue] LA CONSULTORÍA:\n UNA PROFESIÓN DE ÉXITO\n PRESENTE Y FUTURO<<root>>
--[#lightgreen] ¿Qué es lo que\n pretende? 
--- Situar la consultoría\n en el contexto empresarial\n en el que nos movemos
--[#lightgreen] ¿Qué es la consultoría? 
--- La consultoría nace porque\n existen grandes organizaciones\n que internamente tienen\n mucha complejidad
---- y<<sep>>
----- requieren que sistemas de información\n les den soporte a esos procesos\n que son internamente complejos
--- Existe una atención continua\n en el mercado que obliga\n a que las empresas evolucionen
---- Pero <<sep>>
----- Si las empresas no evolucionan,\n estas desaparecen
---- Entonces<<sep>>
----- Tienen que estar en una\n constante remodelación interna\n de procesos o la competencia\n los obliga a ellos
------ Así mismo<<sep>>
------- Tienen que estar operando a\n aquello que les da de comer
-------- Si no puenden hacer las\n dos cosas a la vez<<sep>>
--------- Recurren a empresas que\n les ayuden a transformarse\n o a operar su negocio actual
--- Su papel fundamental <<sep>>
---- Es la presatación de servicios\n de alto valor añadido que\n realizan las empresas consultoras\n y que habitualmente tienen tres\n características fundamentales.
----- Sus características deben ser:<<sep>>
------ Un conocimiento sectorial importante,\n un conocimiento técnico importante\n por encima de lo que realmente está\n implantado y hay que tener una capacidad\n constante de plantear soluciones a los\n problemas del mundo empresarial
**[#lightgreen] Tipo de servicios que se suelen\n prestar en una consultoría
***[#salmon] Consultoría
**** Se desarrollan actividades que tienen\n que ver con la ingeniería o reingeniería\n de procesos de la empresa
***** Es decir<<sep>>
****** Como ayudar a las empresas a que organicen\n sus activiades siguiendo un determinado\n esquema de funcionamiento de manera que\n sea mas óptimo del que tienen en la actualidad,\n atendiendo a los cambios que suelen haber\n con cierta predecidad en estas empresas
**** Otra de las actividades típicas del\n área de consultoría es lo que tiene\n que ver con la analítica avanzada de\n datos, pues hoy en día la información\n es crucial para las empresas, es muy\n importante que estas dispongan en\n tiempo real la información que les\n permita tomar decisiones.
**** Analítica avanzada de datos <<sep>>
***** Ahora es muy frecuente que\n analizando los comentarios\n que se hacen sobre las empresas\n en las redes sociales, la empresa\n perciba cual es el sentimiento que el\n mercado tiene sobre su propia empresa.
****** Esto es denominado\n SentimentalAnalysis\n y es uno de los\n servicios típicos que\n se llevan acabo
******* Requiere<<sep>>
******** De la colaboración de tecnólogos\n que son capaces de conectar las\n redes sociales a estudiar con los\n sitemas que los explotan y luego\n aplicar los algoritmos de Machine\n Learning que son capces de\n enumerarte el sentimiento que\n los clientes reflejan en la red sobre\n tu desempeño en el mercado.
**** Típico en consultoría <<sep>>
***** Como puedes mejorar tus\n canales de atención usando\n las redes sociales
**** Bigdata<<sep>>
***** No ofrece nada que no se pueda\n hacer ya con otras tecnologias,\n pues así se puede hacer de manera\n más barata y de manera más rápida
****** Pero <<sep>>
******* Hay que hacer uso adecuado de la\n misma porque como tecnología\n es mas dura
**** Desarrollo de modelos analíticos\n avanzados productivos utilizando\n las tecnologías o los métodos\n estadísticos o bien los métodos\n basados en inteligencia artificial
***[#salmon] Integración
**** Toda empresa tiene un mapa de\n sistemas con el que da soporte\n a su actividad de negocio
***** Ese mapa de sistemas <<sep>>
****** Por un lado habla de piezas\n software y por otro habla de\n piezas hardware o de\n infraestructura y comunicaciones
******* Además<<sep>>
******** Requiere evolución continua
**** Es necesario que los sistemas\n de gestión de la relación con los\n clientes que utilizan las empresas\n estén conectados con las\n redes sociales
**** Surgen muchos problemas<<sep>>
***** Pues en la entrada del mundo móvil suele aparecer\n la necesidad de que cuando por ejemplo un proceso\n de contratación de un servicio lo movilice hace que\n el vendededor pueda ofrecer la posibilidad de que el\n contrato lo firme en el momento en el que presencialmente\n está atendiendo surgen temas como la firma biométrica o\n como la certificación de sms o correo que garantice la\n autenticidad de la documentación que se está mensajeneando online
****** Esto suele suponer<<sep>>
******* Añadir estas capas al\n mapa de sistemas de\n las compañías
**** Aparición de apps<<sep>>
***** Puedes usarlas estando\n con frecuencia en todos\n los sectores
****** La banca, las telecomunicaciones\n absolutamente en todos\n los casos
*****[#orange] Casos
****** Desarrolo y prueba de apps
****** Movilizar aplicaciones\n que ya existen
**** Aparecen servicios como\n introducir nuevas fuentes\n de datos detro de los mapas\n de sistemas muchas veces\n estructurados
***** Por Ejemplo <<sep>>
****** El registro de las llamadas\n que efectúan los clientes a sus\n empresas de telecomunicación
**** Desarrollo de cuadros de\n manda que permitan la toma\n de decisiones es uno de los\n ámbitos mas demandados
**** Intgración de los entornos relacionales,\n de los estontornos operacionales que ya\n tienen implantadas las organizaciones con\n los nuevos entornos bigdata es algo fundamental
***** En el ámbito de absurcen se está\n demandando la capacidad de disponer\n del servicio gestionado que sea capaz\n de explotar y mantener los modelos\n analíticos que utiliza la compañía
***[#salmon] Externalización
**** Gestión de las aplicaciones <<sep>>
***** Se da cuando las empresas deciden\n elevar todo el mantenimiento y la evolución\n de sus sistemas en un tercero y lo regula\n por un acuerdo de nivel de servicios parametrizado\n por complejidades y diferentes tipos\n de medidas y lo establece por varios años
****** De esta manera<<sep>>
******* Se evita el problema de la\n gestión interna de los recursos
**** Hay compañías que\n demandan la función\n de Comunity Manager\n como un servicio\n gestionado y\n estertanilzado\n a largo plazo
***** También <<sep>>
****** Hay compañías que ya\n se han dado cuenta que\n ofrecer un canal móvil a sus\n clientes está muy bien, que\n darles una app con la que\n se autogestionen es\n efectivamenet algo que\n tienen que hacer
******* Pero<<sep>>
******** Se encuentra un problema\n fundamental y es que la\n diversidad de terminales es\n enorme, la capacidad que deben\n tener para asegurar que los\n cambios interminales de los\n sistemas operativos de las\n terminales en las tiendas\n le ofrecen al cliente una calidad\n de uso suficiente
********* Para ello<<sep>>
********** Requieren una operación\n de aplicaciones móviles\n que les garantice esta\n calidad de servicio
**[#lightgreen] Introducción del mundo movil\n de las apps en el contexto\n empresarial al CIO
*** Ponen un problema porque el\n ya tiene definidos los servicios\n que presta a su organización,\n idealmente ya ha implantado unas\n mejores practicas para gestionar\n esos servicios de manera adecuada.
**** El mundo móvil\n es rompetodo<<sep>>
***** Lo que hay que hacer en primer lugar\n en lo que te demandan es definir que\n papel tiene la entrada de la tecnología\n móvil y de las aplicaciones dentro de\n la organización y como lo voy a gorbenar
****** Es decir<<sep>>
******* Como voy a asegurar que\n le puedo seguir prestando los\n servicios con la calidad esperada\n utilizando este nuevo canal
**[#lightgreen] Transformación digital de\n las empresas
*** implica<<sep>>
**** Revisar como las empresas realizan\n sus activiadades y en la medida de lo\n posible utilizar nuevas tecnologías para\n automatizarlas o para que las puedan\n desempeñar de una manera mas dinámica.
***** Pues<<sep>>
****** El como utilizar dispositivos móviles y\n aplicaciones para gestionar procesos de\n negocio simplificando esos procesos es\n también una actividad que ahora se toma\n muy en cuenta en el ámbito de la\n ingeniería de procesos
**[#lightgreen] El cloud
*** Podemos encontrarnos en\n servicios de consultoría
**** por ejemplo<<sep>>
***** El definir o ayudar a definir\n a las empresas que utilización\n de la nube van a hacer
****** Pero<<sep>>
******* Hay que ver en que\n modalidad la uso
*** Cuando uno tiene su infraestructura\n ubicada en su cpd tiene unos\n procedimientos muy definidos para\n manejarlo cuando no lo ve, cuando\n lo tiene ubicado quien sabe donde\n tiene que tener unos procesos\n operativos diferentes para\n sentirse confortable con ello.
**** En el ámbito de integración<<sep>>
***** Lo que se hace es implantar\n las estrategias cloud que\n previamente se definen\n en el ámbito de consultoría,\n el desarrrollo de herramientas\n que permitan la provisión de\n esas facilidades de la nube.
****** Con mas frecuencia cada vez\n el desarrollo de las aplicaciones\n en la nube y hay algunos productos\n y algunas propuestas para que\n se puedan desarrollar aplicaciones\n directamente de la nube
**[#lightgreen] Servicios de Estrategia
***[#violet] Ayudar a las compañías a\n definir la organización interna\n que han de tener para ser\n mas operativos
**** Con el ámbito de la prestación\n o transformación digital<<sep>>
***** Es necesario definir como\n las compañías se organizan para\n llevar acabo esos procesos de\n manera eficiente
***[#violet] Definición de planes o\n modelos de negocio nuevos
**** A esto nos enfrentamos en la\n actualidad y en los próximos\n años de manera muy clara,\n pues los modelos de negocio\n cambian, o te adaptas o mueres
***[#violet] Estudios de mercado, de\n segmentación de clientes o\n definición de productos\n y servicios
**[#lightgreen] ¿Porqué es una profesión de futuro?
*** La consultoría es un\n modo de vida que tiene\n sus implicaciones 
**** También<<sep>>
***** ofrece retos, en función del\n nivel de retos que quieras\n asumir pues vas a crecer\n mas en tu carrera profesional\n y vas a tener una retrovisión\n mas alta.
*** Es una profesión de futuro\n porque es una profesión\n que te permite alcanzar la\n felicidad profesional
**** Así también<<sep>>
***** La distribución de ingresos\n está viendose muy marcada\n por unas tendencias claras
*** La demanda de la consultoría\n es muy amplia y en la mayoría\n de los casos en las empresas\n van a haber profesionales\n demandados, lo que\n se conoce como\n habilitadores digitales
**[#lightgreen] ¿Qué exige la consultoría?
*** La consultoría es un ámbito\n en el que nos enfrentamos a\n un reto constante, situaciones\n de inmadurez que hay que\n madurar pero que se vuelven\n inmaduras con el tiempo
**** Por tanto<<sep>>
***** Los profesionales que se\n involucran en este ámbito de\n trabajo tienen que ser profesionales\n con una serie de aptitudes e\n inequívocamente con un gran compromiso
****** Esto no es sencillo,\n por lo mismo<<sep>>
******* Se requieren buenos profesionales\n en cuanto a la formación que tienen,\n la experiencia en tecnologías de la\n información pero que además tengan\n una gran capacidad de\n trabajo y evolución
******** Esto es un esfuerzo constante\n para conseguir al final los\n resultados que se necesitan
********* Pero<<sep>>
********** También hay que trabajar\n siempre en equipo, por lo\n mismo hay que ser capaces\n de integrarse a un equipo para\n lograr objetivos comunes y sobre\n todo hay que ser honestos y\n sinceros, traladar y gestionar las\n espectativas de lo que es alcanzable\n y lo que no en todo momento
***[#purple] Evoloción natural que sufren\n o que tienen los profesionales\n en consultoría
**** es<<sep>>
***** Es pasar de ser unos\n gestores de la técnica\n  a progresivamente ser gestores\n de personas
***[#purple] En cuanto actitud
**** Lo que se se requiere es <<sep>>
*****[#pink] Proactividad
****** Se necesita implicación iniciativa\n y anticipación para poder desempeñar\n con calidad la profesión
*****[#pink] Se necesita voluntad\n y mejora
****** Saber que lo que hemos\n avanzado hasta el momento\n va a dejar de ser válido\n probablemente en poco tiempo
******* por tanto<<sep>>
******** Debemos aprender que tenemos\n que estar siempre ligando en\n como mejorar, como innovar\n aportando ideas que contribuyan\n a esa mejora y no solo ligandonos\n a hacer una actividad completa
*****[#pink] Responsabilida que\n lleva la consultoría
****** La capacidad de asumir los\n trabajos para hacerlos y de\n no devolver problemas, hay\n que buscar soluciones con\n autonomía para resolver\n aquello a lo que nos enfrentamos
@endmindmap
```
[//]: # "********************************************************************************************"

```plantuml
@startmindmap
<style>
mindmapDiagram {
  .node {
    BackgroundColor violet
    HorizontalAlignment center
  }
  .sep{
      BackgroundColor lightblue
  }
}
</style>
*[#red] CONSULTORÍA DE SOFTWARE <<root>>
**[#orange] Desarrollo de software
*** Comienza cuando un\n clente tiene una necesidad\n en su negocio
**** Lo primero que haces es lo\n que se conoce como consultoría\n y es hablar con el cliente para ver\n sus necesidades y de acuerdo a\n ello se hace el desarrollo
***** Una vez que efectivamente\n es viable ese proyecto se\n hace lo que es el diseño funcional
****** Diseño funcional<<sep>>
******* Consiste en ver que\n información necesita ese\n software y que información\n ese sistema te tiene que dar
******** Esa información es lo que\n se almacena en la base de\n datos de una manera técnica
******* Lo que construye es que\n modelo de datos vamos a tener\n y además cuantos procesos va\n a tener ese sistema así como\n también lo que se hace es\n hacer un prototipo para\n mostrarle al cliente 
******* Ahora se hace el Diseño técnico<<sep>>
******** Esta es la parte en la que\n se aisla el tipo de servicio\n de cliente o de negocio\n de la parte técnica
********* Aquí<<sep>>
********** Lo que hace el técnico es\n trasladar la necesidad a nivel\n técnico con un lenguaje de\n programación en un entorno\n de hardware
*********** En sí<<sep>>
************ Lo que hace el diseñador\n técnico es crear todos\n los procesos y crear todas\n las necesidades de datos
******** Después hacen su trabajo\n los programadaores<<sep>>
********* los programadores crean ese\n software con ese lenguaje que\n el diseñador técnico decide.
********* Una vez finalizado y\n probado el software<<sep>>
********** La empresa decide que ese\n producto cumple con todos\n aquellos requerimientos tanto\n técnicos como funcionales que\n el cliente ha pedido se lo\n pasan al cliente y cuando\n el cliente dice que está\n bien, se implementa.
******* Influye mucho el propio proyecto\n si el proyecto es un proyecto\n completamente nuevo o si\n es un proyecto evolutivo
******** Pero<<sep>>
********* Podemos estar hablando entre\n un 15 o 20% máximo de tiempo\n lo que es la parte de requisitos\n y la parte de prueba funcional
*** Para realizar un proyecto\n se hace uso de la metodología\n de gestión de proyecto
**** Para ello<<sep>>
***** Se necesita de un jefe de proyecto
****** El jefe de proyecto<<sep>>
******* Es la persona encargada\n de planificar las fases de\n ese proyecto, de ver que\n recursos se necesitan para\n ejecutar ese proyecto, de\n hablar con el cliente en que\n momento van a necesitarlo\n para que defina los requisistos\n o hacer la prueba del proyecto
******** También<<sep>>
********* Se encarga de coordinar todas\n las tareas y dotar a ese proyecto\n de los recursos humanos necesarios\n en cada momento para poder\n ejecutar las tareas y el plan
****** Esto es muy importante<<sep>>
******* Si el jefe de proyecto no cumple\n los hitos marcados con el cliente,\n el cliente no va a estar satisfecho\n porque no van a hacer el proyecto\n a tiempo y no va a garantizar que\n efectivamente el proyecto se cumpla
**[#orange] ¿Qué se hace en una\n consultora tecnológica?
*** Realizan todas aquellas\n tareas que permiten crear\n o mantener lo que se\n conoce como informática\n de una empresa
**** Es así que<<sep>>
***** Se dedican a dar servicios\n a una empresa no a particulares
***[#green] ¿Qué es lo que necesita\n una empresa en el plano\n informático para comenzar\n a funcionar y mantenerse?
**** Para empezar a trabajar en\n una empresa que no tiene\n nada de informática hay que\n crear toda la parte de infraestructura
**** La infraestructura<<sep>>
***** Es todo aquello que es\n hardware, aparatos como\n ordenadores, servidores\n que son lugares donde\n alamacenas información.
****** Además<<sep>>
******* se necesita habilitar toda la\n infraestructura de comunicaciones\n que nos van a permitir comunicarnos\n con el exterior, así como también dotarla\n de todo aquello que nos permita mantener\n segura toda la información\n que vamos a almacenar
******** y<<sep>>
********* Después de aplicaciones\n que le permitan a esas empresas\n desarrollar su negocio
***[#green] ¿Cómo se atiende a una empresa\n de grandes dimensiones frente\n a una empresa de menores dimensiones?
@endmindmap
```
[//]: # "********************************************************************************************"

```plantuml
@startmindmap
<style>
mindmapDiagram {
  .node {
    BackgroundColor lightgreen
    HorizontalAlignment center
  }
  .sep{
      BackgroundColor lightblue
  }
}
</style>
*[#pink] APLICACIÓN DE LA INGENIERÍA DE SOFTTWARE<<root>>
@endmindmap
